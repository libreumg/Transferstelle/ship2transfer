#' generate the sql code for the variables
#'
#' @param df_variable the variable data frame
#'
#' @return the sql code
#' 
#' @author Jörg Henke
#' 
util_generate_variable_sql <- function(df_variable) {
  df_variable$data_type <- util_replace_string(df_variable$data_type, "float", "'metrisch'")
  df_variable$data_type <- util_replace_string(df_variable$data_type, "string", "'text'")
  df_variable$data_type <- util_replace_string(df_variable$data_type, "integer", "'nominal'")
  df_variable$data_type <- util_replace_string(df_variable$data_type, "datetime", "'Datum/Uhrzeit'")
  df_variable$data_type <- util_replace_string(df_variable$data_type, "jsonb", "'text'")
  
  sql1 <- "  insert into transfer.t_dd_variable(
    key_parent_element, order_nr, short_name, foreign_id, public,
    desc_long_de, desc_long_en, note_de, note_en, var_niveau,
    valuelist_de, valuelist_en
  ) select key_element, %s, %s, '%s', %s, %s, %s, %s, %s, %s, %s, %s
    from transfer.t_dd_element_hierarchy
    where foreign_id = %s
    on conflict (foreign_id)
    do update
    set order_nr = excluded.order_nr,
        short_name = excluded.short_name,
        key_parent_element = excluded.key_parent_element,
        public = excluded.public,
        desc_long_de = excluded.desc_long_de,
        desc_long_en = excluded.desc_long_en,
        note_de = excluded.note_de,
        note_en = excluded.note_en,
        var_niveau = excluded.var_niveau,
        valuelist_de = excluded.valuelist_de,
        valuelist_en = excluded.valuelist_en;"
  
  part1 <- sprintf(sql1, df_variable$order_nr, df_variable$short_name,
                   df_variable$foreign_id, df_variable$public,
                   df_variable$desc_long_de, df_variable$desc_long_en,
                   df_variable$note_de, df_variable$note_en,
                   df_variable$data_type, df_variable$valuelist_de,
                   df_variable$valuelist_en, df_variable$parent_foreign_id)
  
  sql2 <- "  update transfer.t_dd_element_hierarchy set summary = 1 where foreign_id = %s;"
  
  part2 <- sprintf(sql2, unique(df_variable$parent_foreign_id))
  return(c(part1, part2))
}